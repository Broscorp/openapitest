openapi: 3.0.2
info:
  title: Flightpos Registration API
  version: 1.2.7
  description: Connector for providing registration information to the POS.
servers:
  -
    url: 'https://dev.flightpos.com:8080/registration'
    description: ''
paths:
  '/v1/device/{deviceNumber}':
    summary: Request –  Device Number (v1)
    description: |-
      Gives the details of a device.
      &
      Deregisters the device.
    get:
      tags:
        - device-info
      parameters:
        -
          name: deviceNumber
          schema:
            type: string
          in: path
          required: true
      responses:
        '200':
          $ref: '#/components/responses/DeviceDataRSResponse'
        '400':
          description: There is a problem with the data passed it.
        '422':
          description: >-
            The server understands the content type of the request but was unable to process the
            contained instructions. An invalid or unknown deviceNumber passed. It does this sooner
            than a 404.
      summary: Retrieve Device information.
      description: Gives the details of a device.
    delete:
      tags:
        - device-registration
      parameters:
        -
          name: deviceNumber
          schema:
            type: string
          in: path
          required: true
      responses:
        '200':
          description: >-
            The server has fulfilled the request and the user agent SHOULD reset the document view
            which caused the request to be sent. The server is not returning any content.
        '204':
          description: 'The server successfully processed the request, but the device is unknown.'
        '400':
          description: There is a problem with the data passed it.
      summary: Deregister device.
      description: Deregisters the device.
  /v1/devices/lastseen:
    summary: When was a device last seen.
    description: When was a device last registered.
    get:
      tags:
        - device-info
      responses:
        '200':
          $ref: '#/components/responses/DeviceLastSeenResponse'
      summary: When did devices register.
  /v1/device/operators:
    summary: Request –  Device Operators (v1)
    description: Gives the id codes of operators
    get:
      tags:
        - operator-info
      responses:
        '200':
          $ref: '#/components/responses/OperatorResponse'
        '204':
          description: 'The server successfully processed the request, but the device is unknown.'
        '400':
          description: There is a problem with the data passed it.
      summary: Operators registered.
      description: Gives the id codes of operators
  '/v1/device/licences/{operatorIata}':
    summary: Request –  Device Licences (v1)
    description: Gives the licence information for a client by IATA.
    get:
      tags:
        - operator-info
      parameters:
        -
          name: operatorIata
          schema:
            type: string
          in: path
          required: true
      responses:
        '200':
          $ref: '#/components/responses/LicenceDataResponse'
        '204':
          description: The URL may be wrong it must end with an operator code.
        '400':
          description: There is a problem with the data passed it.
        '404':
          description: The URL may be wrong it must end with a '/'.
        '422':
          description: Invalid operator IATA passed (All upper case?).
        '424':
          description: 'The server successfully processed the request, but the client is unknown.'
      summary: Licences for operators
      description: Gives the licence information for a client by IATA.
    parameters:
      -
        name: operatorIata
        schema:
          type: string
        in: path
        required: true
  '/v1/device/endpoints/{operatorIata}':
    summary: Operator Endpoints
    description: Gives the endpoints for a client
    get:
      tags:
        - operator-info
      parameters:
        -
          name: operatorIata
          schema:
            type: string
          in: path
          required: true
      responses:
        '200':
          $ref: '#/components/responses/EndpointResponse'
        '204':
          description: The URL may be wrong it must end with an operator code.
        '400':
          description: There is a problem with the data passed it.
        '404':
          description: The URL may be wrong it must end with a '/'.v
        '422':
          description: >-
            The server understands the content type of the request but was unable to process the
            containedinstructions. An invalid operator IATA passed (All upper case?).
      summary: Operator Endpoints
      description: Gives the endpoints for a client
    parameters:
      -
        name: operatorIata
        schema:
          type: string
        in: path
        required: true
  /v1/device/activate:
    summary: Register device (v1)
    description: |-
      Register the device passed in.
      This is a V1 response and is the same as the /REST/device/activate endpoint.
    put:
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Activation'
        required: false
      tags:
        - device-registration
      responses:
        '200':
          $ref: '#/components/responses/DeviceRegistrationResponseV1'
        '204':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: The key can not be found.
        '400':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: If the body data is not passed correctly.
        '406':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: If the body data is not passed correctly. -- Invalid data Json
        '422':
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: >-
            There is a problem with the data passed. The server understands the content type of
            the request entity , and the syntax of the request entity is correct. but was unable
            to process the contained instructions. This could be because of there being no
            matching licences or the device is registered using a different code.
        '423':
          description: All licences for this key have been registered already.
      summary: Register device (v1)
      description: Register the device passed in
  /v1/device/endpoints:
    summary: All Endpoints
    description: Gives the endpoints known.
    get:
      tags:
        - operator-info
      parameters:
        -
          name: activationCode
          description: |-
            The activation code to filter on.
            Only endpoints with this activationCode will be returned.
          schema:
            type: string
          in: query
      responses:
        '200':
          $ref: '#/components/responses/EndpointResponse'
        '204':
          description: The URL may be wrong it must end with an operator code.
        '400':
          description: There is a problem with the data passed it.
        '404':
          description: The URL may be wrong it must end with a '/'.v
        '422':
          description: >-
            The server understands the content type of the request but was unable to process the
            containedinstructions. An invalid operator IATA passed (All upper case?).
      summary: All Endpoints
      description: Gives the endpoints known.
    parameters:
      -
        name: activationCode
        description: |-
          The activation code to filter on.
          Only endpoints with this activationCode will be returned.
        schema: {}
        in: query
  /v2/device/activate/:
    summary: Register device (v2)
    description: Register the device passed in
    put:
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Activation'
            examples:
              From Postman:
                value:
                  activationCode: local000
                  deviceNumber: postman
        required: false
      tags:
        - device-registration
      responses:
        '200':
          headers:
            Access-Control-Allow-Origin:
              schema:
                type: string
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DeviceRegistrationV2'
          description: |-
            Device has successfully registered and a new registration was created for it
            
            This is different from 200 cause 200 means Reregistered successfully.
        '201':
          headers:
            Access-Control-Allow-Origin:
              schema:
                type: string
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DeviceRegistrationV2'
          description: |-
            Device has successfully registered and a new registration was created for it
            
            This is different from 200 cause 200 means Reregistered successfully.
        '400':
          headers:
            Access-Control-Allow-Origin: {}
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
          description: If the body data is not passed correctly.
        '401':
          $ref: '#/components/responses/ErrorResponse'
        '403':
          $ref: '#/components/responses/ErrorResponse'
      summary: Register device (v2)
      description: Register the device passed in
components:
  schemas:
    LicenceData:
      description: ''
      type: object
      properties:
        used:
          description: How many licences are used.
          type: integer
        available:
          description: How may licences are available.
          type: integer
        devices:
          description: All the devices that are registered to this device.
          type: array
          items:
            $ref: '#/components/schemas/ActivatedDevice'
        operatorIATA:
          description: The IATA of the airline for this device.
          type: string
    ConfigurationValue:
      description: A single configuration KEY and VALUE.
      type: object
      example:
        key1: val1
    CardReaderSettings:
      description: 'Card Reader Settings see: https://docs.adyen.com/point-of-sale/terminal-api-reference'
      required:
        - apiKey
        - poiid
        - encryptionKey
        - password
        - version
      type: object
      properties:
        apiKey:
          description: Credentials to authenticate the API requests that you make to Adyen.
          type: string
        poiid:
          description: Identification of a payment terminal for the NEXO Sale to POI protocol.
          type: string
        encryptionKey:
          description: Required in Adyen API calls.
          type: string
        password:
          description: Required in Adyen API calls.
          type: string
        _meta:
          $ref: '#/components/schemas/MetaData'
          description: ''
        version:
          description: Used in Adyen API calls.
          type: string
    Activation:
      description: ''
      type: object
      properties:
        deviceNumber:
          description: |-
            This is the licence that is being used to register the device.
            It can not be "blank",
            It must me "alphanumeric",
            It must be less than 45 characters long.
          type: string
          example: MyDev
        activationCode:
          description: |-
            This is the licence or activation code that is being used to register the device.
            It can not be "blank",
            It must me "alphanumeric",
            It must be less than 45 characters long.
          type: string
          example: '123456'
    ActivatedDevice:
      description: A record of a registered device.
      type: object
      properties:
        airlineCode:
          description: The airline IATA code.
          type: string
        deviceNumber:
          description: The device number sent during registration.
          type: string
        dateActivated:
          description: When the device was activated.
          type: string
    DeviceLastSeen:
      description: When a device last registered.
      required:
        - deviceNumber
        - when
      type: object
      properties:
        deviceNumber:
          description: The devices registered number.
          type: string
        when:
          format: date-time
          description: When the device last connected.
          type: string
    Endpoint:
      description: ''
      required:
        - operatorIata
        - name
        - url
      type: object
      properties:
        operatorIata:
          description: ''
          type: string
        name:
          description: ''
          type: string
        url:
          description: ''
          type: string
    Operator:
      description: ''
      required:
        - operatorIata
        - name
      type: object
      properties:
        operatorIata:
          description: ''
          type: string
        name:
          description: ''
          type: string
    DeviceRegistrationV1:
      description: ''
      type: object
      properties:
        deviceNumber:
          description: This is the device number of the device that posted the configuration request.
          type: string
        operatorIata:
          description: This is the operator IATA for the device the configuration belongs to.
          type: string
        keys:
          $ref: '#/components/schemas/AWSCredsV1'
          description: Temporary AWS credentials
        endpoints:
          description: "This is the list of available endpoints as provided by the configuration server.\r\nThis data will vary depending on the licence number.\r\n"
          type: array
          items:
            $ref: '#/components/schemas/AvailableEndpoint'
        config:
          $ref: '#/components/schemas/ConfigurationSetV1'
          description: ''
      example:
        deviceNumber: BMVSBB
        operatorIata: FPS
        keys:
          key: AKIAQYYCDFUTGBNJNVLL
          secret: KBCeOHYZSyizLiCOTFkLmJMTFoeavgfDk5TG/tt1
        endpoints:
          -
            name: 'FPASettings:FPARootURL'
            url: 'https://sandbox.flightpos.com/dev-paymentgateway'
          -
            name: 'FPASettings:RegistrationRootURL'
            url: 'https://sandbox.flightpos.com/dev-register-server'
          -
            name: 'odysseus:host'
            url: 'http://localhost:8080'
          -
            name: 'odysseus:supportData'
            url: 'https://sandbox.flightpos.com/dev-pegasus-support-data'
        config:
          stage: '72'
          appstage: '72'
          'FPASettings:FPARootURL': 'https://sandbox.flightpos.com/dev-paymentgateway'
          'FPASettings:RegistrationRootURL': 'https://sandbox.flightpos.com/dev-register-server'
          configData: '2022-02-19T11:42:05.956Z'
          'FPASettings:AllowedCreditCardRanges': 34;37;2221-2720;51-55;4
          'FPASettings:BlockedCreditCards': 4530910000012345;370123456789017;539123
          'FPASettings:PublicAccessKeyName': '20190523'
          'FPASettings:FPAPublicKey': MII....QAB
          'FPASettings:UploadUrlEndpoint': TBA
    DeviceRegistrationV2:
      description: ''
      type: object
      properties:
        deviceNumber:
          description: This is the device number of the device that posted the configuration request.
          type: string
        operatorIata:
          description: This is the operator IATA for the device the configuration belongs to.
          type: string
        configuration:
          $ref: '#/components/schemas/ConfigurationSetV2'
          description: The configuration values needed by the registered device.
      example: FPS
    MetaData:
      description: Data about the data in the parent node.
      type: object
      additionalProperties:
        type: object
    WifiSettings:
      description: Access credentials for the Wifi network.
      required:
        - ssid
        - password
      type: object
      properties:
        ssid:
          description: >-
            SSID stands for Service Set IDentifier and is your network’s name. If you open the
            list of Wi-Fi networks on your laptop or phone, you’ll see a list of SSIDs. Wireless
            router or access points broadcast SSIDs so nearby devices can find and display any
            available networks.
          type: string
        password:
          description: The WIFI network password.
          type: string
      additionalProperties:
        type: object
    Error:
      title: Root Type for Error
      description: ''
      type: object
      properties:
        error:
          description: What went wrong.
          type: string
      example:
        error: 'Activation code does not match any Licence not found for any operator:local000'
    OdysseusSettings:
      title: Root Type for OdysseusSettings
      description: Odysseus credentials and host name
      required:
        - wsUrl
        - apiUrl
        - apikey
      type: object
      properties:
        apikey:
          description: api key needed to access Odysseus.
          type: string
        apiUrl:
          description: host name or ip of odysseus
          type: string
        wsUrl:
          description: ws endpoint url
          type: string
      example:
        apikey: tbc-api-key
        apiUrl: 'http://odysseus'
        wsUrl: 'ws://odysseus'
    ConfigurationSetV1:
      description: The configuration added to a V1 request by the Node Lambda configuration server.
      type: object
      example:
        stage: '72'
        appstage: '72'
        'FPASettings:FPARootURL': 'https://sandbox.flightpos.com/dev-paymentgateway'
        'FPASettings:RegistrationRootURL': 'https://sandbox.flightpos.com/dev-register-server'
        configData: '2022-02-19T10:34:59.312Z'
        'FPASettings:AllowedCreditCardRanges': 34;37;2221-2720;51-55;4
        'FPASettings:BlockedCreditCards': >-
          4530910000012345;370123456789017;5191330000004415;5168834782500580;4462918758434035;4482330052209301;4530910000012345;370123456789017;5191330000004415;5555555555554444;5105105105105100;4111111111111111;4012888888881881;6011000400000000;3528000700000000;5454545454545454;4444333322221111;4917610000000000;4462030000000000;4484070000000000;6011601160116611;6445644564456445;3569990010095840;5101180000000000;2222400070000000;5100290029002909;5555341244441110;5577000055770000;5136333333333330;5585558555855583;5555444433331111;2222410740360010;2222410700000002;2222400010000008;2223000048410010;2222400060000007;2223520443560010;5500000000000004;2222400030000004;6771798025000004;5100060000000002;5100705000000002;5103221911199245;5424000000000015;2222400050000009;5106040000000008;4111111145551142;4988438843884305;4166676667666746;4646464646464644;4000620000000007;4000060000000006;4293189100000008;4988080000000000;4001590000000001;4000180000000002;4000020000000000;4000160000000004;4002690000000008;4400000000000008;4484600000000004;4607000000000009;4977949494949497;4000640000000005;4003550000000003;4000760000000001;4017340000000003;4005519000000006;4131840000000003;4035501000000008;4151500000000008;4571000000000001;4199350000000002;4212345678910006;5201285565672311;5201287499052008;5201281592331633;5201288822696974;5201289500843268;4212345678901237;5212345678901234;5204247750001471;4761120010000492;4001919257537193;4007702835532454;4917484589897107;4263982640269299;2222420000001113;5425233430109903;2222990905257051;2223007648726984;2223577120017656;5111010030175156;5185540810000019;5200828282828210;5204230080000017;5204740009900014;5420923878724339;5455330760000018;5506900490000436;5506900490000444;5506900510000234;5506920809243667;5506922400634930;5506927427317625;5553042241984105;5555553753048194;6011000990139424;6011111111111117;5458328219687780;5506924231097510;5506924323573490;5506923615306240;5555555555558720;5555555555558740;5555555555550140;5313130112075490;5200828282828210;5555553753048190;5111010030175150;5506924295804790;5506927427317620;5506920809243660;5506922400634930;371449635398431;378734493671000;378282246310005;451002;516483;538423;538640;538649;538651;377852;377877;377850;377767;377878;377766;337879;377187;370286;372300;377876;377765;403995;406583;435880;464556;481774;516488;529263;538414;538641;538435;538438;538436;516645;374327;374329;379016;379039;400035;400487;403027;405976;407714;410106;421410;421657;424934;428235;433702;434267;434493;440847;451016;457758;467883;469396;469708;471849;480989;483588;484718;491342;494159;494160;516499;516523;517510;519503;523150;523716;526293;526430;526791;530029;533211;535420;538405;538412;538415;538422;538424;538427;538429;538459;538600;538601;538608;538642;538650;538680;538688;538691;539925;402784;404904;408277;446053;469137;473256;531496;533873;539736;538644;538648;538413;538428;538437;426639;501007;559202;560251;560254;560265;560279;579974;584003;601149;603634;416202;428380;375414;375415;375416;376023;377642;377644;379226;379227;379228;379229;356889;374326;377132;379987;401195;401367;401796;403015;409766;409767;414565;421583;421595;421689;422390;422630;426579;430589;434675;436415;444052;446278;451766;453177;453183;453215;456405;458440;461699;461700;462230;462287;462288;462339;463206;464005;464993;467785;471295;471470;473252;483446;483796;484680;484777;489385;490840;510481;516383;516392;516393;516795;516907;517417;518568;518680;518860;521102;522184;525610;526051;526414;526906;530515;530572;530795;532713;535018;535476;536347;536470;536895;537940;538652;538653;539921;544343;544345;546327;557389;557692;557755;557791;557920;456571;517574;538653;516527;538738;486399;528837;537941;377155;377677;538444;521465;542172;520416;379019;535522;516529;557362;530331;516281;520012;459241;538435;541755;527295;538666;528836;541264;554591;529913;472684;516483;530492;517045;540450;528038;538423;510074;414049;530482;559616;538678;447817;538642;538674;476358;554333;421144;490974;538600;459528;549532;519525;417409;550614;517473;538424;538427;539983;405844;440768;516511;528249;466544;548281;467094;528082;527529;416991;545041;532017;533869;459527;538675;532916;533858;557348;457737;531935;425601;528808;538641;538688;517147;419650;447818;425603;540451;530446;530331;533986;534661;545421;549818;554723;541592;538600;557424;542428;530331;408150;554769;527098;535463;529950;459806;535294;516529;471418;511808;513377;435044;459627;459644;459654;459664;539123
        'FPASettings:PublicAccessKeyName': '20190523'
        'FPASettings:FPAPublicKey': >-
          MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzqzRg0YiVLphHW7CfutBP82LjFb+WwRQ2troCxXhzzWxYqODaQDz0fzG/76RsPj0oedm2QFmJzpTIH/2FmRnHZgpnyB6OkTy4UfOaWhqmIQgqASMz+NeY2Wzn1glo6SZickJdiQNHlr2yd5GmAuJb43uE/20UYsrYYtWpraNmTP2THwZbFep2TJ2VL8gFXGF57WCu2MPJ6GM+YF6R9knb7kAL30otjjdkqMvBJSTVAdRZJIXW6z3gdRGcG+/z7Qt4d3Ik6+RUQrSXfFY8+vNokhoTPb8AY2OQD+ntbv8tYa8OrQo28v9EXoZqury9qdMvpcx921jo8Ci+7b7hK2i/wIDAQAB
        'FPASettings:UploadUrlEndpoint': TBA
    AWSCredsV2:
      description: ''
      required:
        - accessKeyId
        - secretAccessKey
      type: object
      properties:
        _meta:
          $ref: '#/components/schemas/MetaData'
          description: ''
        accessKeyId:
          description: |-
            AWS access key.
            The access key ID that identifies the temporary security credentials.
          type: string
        secretAccessKey:
          description: |-
            AWS secret access key.
            The secret access key that can be used to sign requests.
          type: string
        sessionToken:
          description: |-
            The token that users must pass to the service API to use the temporary credentials.
            Set if the AWS credentials are temporary.
          type: string
        expiration:
          format: date-time
          description: The date on which the current credentials expire.
          type: string
    AWSCredsV1:
      description: ''
      required:
        - key
        - secret
      type: object
      properties:
        key:
          description: |-
            AWS access key.
            The access key ID that identifies the temporary security credentials.
          type: string
        secret:
          description: |-
            AWS secret access key.
            The secret access key that can be used to sign requests.
          type: string
    PropertySource:
      description: This describes a service that the process reading this configuration should read.
      required:
        - name
        - type
        - value
      type: object
      properties:
        name:
          description: The source name used to determine where to read the information from.
          type: string
        value:
          description: |-
            A configuration value that should be used when reading the configuration source.
            If there are multiple then the named type should be read several times.
          type: array
          items:
            type: string
        type:
          description: >-
            What the source is and how it should be read.
            
            This describes the "value" field.
            
            If "S3" then data from an S3 bucket should be read. The name is the name of the bucket
            and the value is a file path in the known bucket.
            
            If "STS" then AWS credentials should be read and the value is a label for the
            credentials and the value is the role allocated.
            
            If "URL" then this is an endpoint from which data should be fetched bu a GET request.
            The name is a label and the value the path.
            
            If "JSON" then this is a node of value supplied in addition to the defined
            configuration. The value is a json node of any type.
          enum:
            - S3
            - STS
            - SECRET
            - URL
          type: string
    FPASettings:
      description: Configuration data for use in Pegasus.
      required:
        - RegistrationRootURL
        - FPARootURL
      type: object
      properties:
        RegistrationRootURL:
          description: The URL of the registration server to use.
          type: string
          example: 'https://bluebox.flightpos.com/ryr-register-server-live'
        FPARootURL:
          description: The URL of the Midas payment server.
          type: string
          example: 'https://bluebox.flightpos.com/ryr-test'
        allowedCreditCardRanges:
          description: The credit cards ranges that can be read.
          type: array
          items:
            type: string
          example: ' ["34","37","2221-2720","51-55","4"]'
        blockedCreditCards:
          description: Credit cards that are to be blocked.
          type: array
          items:
            type: string
          example: |
            ["4530910000012345","370123456789017","5191330000004415","5168834782500580"]
        publicAccessKeyName:
          description: The name of the secret key to be provided.
          type: string
          example: '20190523'
        publicKey:
          description: The public key with which to encrypt data to be sent to Midas/FPA.
          type: string
          example: 973124FFC4A03E66D6A4458E587D5D6146F71FC57F359C8D516E0B12A50AB0D9
      example:
        RegistrationRootURL: 'https://sandbox.flightpos.com/dev-register-server'
        FPARootURL: 'https://bluebox.flightpos.com/ryr-test'
    StockDataSettings:
      description: Data needed to aquire stock data.
      required:
        - s3BucketName
      type: object
      properties:
        _meta:
          $ref: '#/components/schemas/MetaData'
          description: ''
        s3BucketName:
          description: The name of the S3 bucket.
          type: string
          example: examplebucket
        rootKeyPath:
          description: |-
            The root folder that will be used together with the sync patterns.
            If not present this is treated as "/" (the root path).
          type: string
          example: /developers
        keyPatterns:
          description: |-
            An array of file paterns to load from the defined path.
            If not present then everything must be synced.
          type: array
          items:
            type: string
          example: '{ "/*.json" }'
    AvailableEndpoint:
      description: ''
      type: object
      properties:
        name:
          description: ''
          type: string
        url:
          description: ''
          type: string
    ConfigurationSetV2:
      description: A set of Key/Value pairs. Each one is a configuration value.
      required: []
      type: object
      properties:
        FPASettings:
          $ref: '#/components/schemas/FPASettings'
          description: Data needed to access the FlightPOS Payment Acceptor “Midas”.
        _additional:
          description: >-
            These are values that are provided as additional configuration data but are outside
            the schema.
          type: object
        _meta:
          description: This is data about the configuration set that is not configuration.
          type: object
        wifiSettings:
          $ref: '#/components/schemas/WifiSettings'
          description: Data needed to connect to the WIFI.
        cardreaderSettings:
          $ref: '#/components/schemas/CardReaderSettings'
          description: ''
        stockDataSettings:
          $ref: '#/components/schemas/StockDataSettings'
          description: ''
        awsCredentials:
          $ref: '#/components/schemas/AWSCredsV2'
          description: Credentials for the device to use to access AWS APIs.
        odysseusSettings:
          $ref: '#/components/schemas/OdysseusSettings'
          description: settings needed to connect to Odysseus
        propertySources:
          description: >
            A set of source services that the system reading the recieving system should check and
            add to its configuration.
          type: array
          items:
            $ref: '#/components/schemas/PropertySource'
  responses:
    LicenceDataResponse:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/LicenceData'
      description: ''
    DeviceDataRSResponse:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ActivatedDevice'
      description: ''
    OperatorResponse:
      content:
        application/json:
          schema:
            type: array
            items:
              $ref: '#/components/schemas/Operator'
      description: ''
    DeviceLastSeenResponse:
      content:
        application/json:
          schema:
            type: array
            items:
              $ref: '#/components/schemas/DeviceLastSeen'
      description: ''
    EndpointResponse:
      content:
        application/json:
          schema:
            type: array
            items:
              $ref: '#/components/schemas/Endpoint'
      description: '[DEPRECATED]'
    DeviceRegistrationResponseV1:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/DeviceRegistrationV1'
      description: ''
    DeviceRegistrationResponseV2:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/DeviceRegistrationV2'
      description: ''
    ErrorResponse:
      content:
        application/json:
          examples:
            No Licence found.:
              value:
                error: >-
                  Activation code does not match any Licence not found for any
                  operator:local000
      description: ''
tags:
  -
    name: device-info
    description: Details related to devices
  -
    name: device-registration
    description: The registration endpoint
  -
    name: operator-info
    description: The airlines/clients
